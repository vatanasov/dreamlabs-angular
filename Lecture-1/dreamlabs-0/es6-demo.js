const test = 1;

function upperScope() {
    var CONSTANT = 1;

    function test1() {
        console.log(CONSTANT);
    } 
}
//console.log(CONSTANT);
//test1();
// test= 2;
//console.log(test);
if (true) {
    let scopedVar = 123;
   // console.log('pass');
}
//console.log(scopedVar);

/*for (let i = 0; i < 3; i++) {
    setTimeout(function() {
        console.log(i);
    }, 0)
}

var what = 'ananas';
var mlText = `Dqdo vadi
 ${what}`;
console.log(mlText);
*/

var array = [1, 2, 3, 4, 5];

/*for (var key of array) {
    console.log(key);
}*/

var fatArrow = (a, b) => {
    console.log(this);
    return a + b;
}



var object = {
    a: 1,
    b: 2,
    getSumFn: function() {
        var _this = this;
        return function() {
            return _this.a + _this.b;
        }
    }
}

console.log(object.getSumFn()());