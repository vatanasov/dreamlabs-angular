import { ListWidget } from "./list-widget";

 window.onload = function() {
   const listWidget: ListWidget = new ListWidget(
       document.getElementById('container')
   );
   
   listWidget.init();
 }