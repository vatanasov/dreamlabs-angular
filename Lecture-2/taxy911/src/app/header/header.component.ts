import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  title = 'Taxi 911';

  isMenuShown = false;

  constructor() { }

  ngOnInit() {
  
  }

  getExtraClass() {
    return this.isMenuShown ? 'show' : '';
  }


  getString(): string {
    return 'Test';
  }

  toggleMenu() {
    this.isMenuShown = !this.isMenuShown;
  }
}
