"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const o = rxjs_1.Observable.create((observer) => {
    console.log('creating observer');
    observer.next(1);
    observer.next(2);
    observer.next(3);
    observer.complete();
    observer.next(4);
    // throw new Error('Nice error');
});
let sum = 0;
const sub = o.pipe(operators_1.map((v) => v + 1))
    .pipe(operators_1.filter((v) => v % 2 == 0))
    .subscribe((v) => {
    console.log(v);
    sum += v;
}, (err) => console.log(err), () => {
    console.log('completed');
});
rxjs_1.of('a').pipe(operators_1.flatMap((v) => rxjs_1.of(v + 'b'))).subscribe((v) => console.log(v));
rxjs_1.forkJoin(rxjs_1.of('c'), rxjs_1.of('d')).subscribe((values) => console.log(values));
console.log(sum);
//# sourceMappingURL=index.js.map