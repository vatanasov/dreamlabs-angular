import {Observable, Observer, Subscription, of, forkJoin} from 'rxjs';
import {map, filter, flatMap} from 'rxjs/operators';

const o: Observable<any> = Observable.create((observer: Observer<any>) => {
    console.log('creating observer');
    observer.next(1);
    observer.next(2);
    observer.next(3);
    observer.complete();
    observer.next(4);
    // throw new Error('Nice error');
});

let sum: number = 0;
const sub: Subscription = o.pipe(map((v) => v + 1))
        .pipe(filter((v) => v % 2 == 0))
        .subscribe((v) => {
    console.log(v);
    sum += v;
},(err) => console.log(err), () => {
    console.log('completed');
});

of('a').pipe(flatMap((v) => of(v + 'b'))).subscribe((v) => console.log(v));

forkJoin(of('c'), of('d')).subscribe((values) => console.log(values));

console.log(sum);