class Person {
    constructor(age, name) {
        this.age = age;
        this._name = name;
    }

    getName() {
        return this.name;
    }
}

class Student extends Person {
    constructor(age, name) {
        super(age, name);
        this.nameChanged = false;
    }
    get name() {
        return "Student " + this._name;
    }

    set name(name) {
        this._name = name;
        this.nameChanged = true;    
    }
}

const person = new Student(20, 'Pesho');

console.log(person);

console.log(Person.prototype.getName);

console.log(person.name);
console.log(person.nameChanged);
person.name = 'Gosho';
console.log(person.nameChanged);
console.log(Student.test)

