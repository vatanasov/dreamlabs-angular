function Person(age, name) {
    this.age = age;
    this.name = name;
}

Person.prototype.getName = function () {
    return this.name;
}

function Student(age, name) {
    Person.call(this, age, name);
}

Student.staticVariable = 'some value';

Student.prototype = Object.create(Person.prototype);

var person = new Person(20, 'Pesho');

console.log(person);