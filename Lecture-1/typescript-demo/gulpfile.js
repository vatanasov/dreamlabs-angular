const gulp = require('gulp');
const concat = require('gulp-concat');
const ts = require('gulp-typescript');
const exec = require('gulp-exec');

const browserify = require('browserify');
const tsify = require('tsify');
const source = require('vinyl-source-stream');

const paths = {
   client: {
      ts: './src/**/*.ts',
      html: './src/*.html'
   },
   tsconfig: 'tsconfig.json',
   dist: 'dist',
   demo: {
      dist: 'demo',
      html: './src/demo/**/*.html'
   }
};


gulp.task('ts', () => {
   return browserify({
      basedir: '.',
      debug: true,
      entries: ['src/main.ts'],
      cache: {},
      packageCache: {}
   })
      .plugin(tsify)
      .bundle()
      .pipe(source('bundle.js'))
      .pipe(gulp.dest(paths.dist));
});


gulp.task('html', () => {
   gulp.src(paths.client.html)
   .pipe(gulp.dest(paths.dist))
});



gulp.task('build', ['ts', 'html']);
gulp.task('default', ['build']);