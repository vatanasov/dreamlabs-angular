import {Person} from './Person';

const test: string = "1";

const object: {key: string, value: number | string} = {
    key: 'price',
    value: undefined
}


class TestClass {
    private name: string = 'TestClass';

    protected someProp: any = 'someProp value';

    public static readonly somePublicProp: any = 'some public proip value';

    public changeValue(): void {
        //TestClass.somePublicProp = "new value";
    }
}

const testInstance = new TestClass;
/*
console.log(testInstance.name);
console.log(testInstance.someProp);

console.log(testInstance.somePublicProp);*/

const person123 = new Person(12, 'Sally');