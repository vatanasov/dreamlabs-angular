var Gender;
(function (Gender) {
    Gender["MALE"] = "male";
    Gender["FEMALE"] = "female";
    Gender["OTHER"] = "other";
})(Gender || (Gender = {}));
var Person = /** @class */ (function () {
    function Person(age, name, gender) {
        if (gender === void 0) { gender = Gender.OTHER; }
        this.age = age;
        this.name = name;
        this.gender = gender;
    }
    Person.prototype.setGirlfriend = function (girlfriend) {
        this.girlfriend = girlfriend;
    };
    Person.prototype.setGender = function (gender) {
        this.gender = gender;
    };
    return Person;
}());
var person = new Person(20, 'Pesho');
person.setGender(Gender.MALE);
console.log(person);
console.log(Gender);
//# sourceMappingURL=Person.js.map