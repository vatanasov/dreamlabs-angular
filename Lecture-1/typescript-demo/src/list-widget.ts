import {fromEvent} from 'rxjs';

export class ListWidget {

    private entries: string[] = [];

    private btn: HTMLButtonElement;

    private input: HTMLInputElement;

    private list: HTMLUListElement;

    constructor(container: HTMLElement) {
        this.btn = container.querySelector('button');
        this.list = container.querySelector('ul');
        this.input = container.querySelector('input');  
    }

    init(): void {
        fromEvent(this.btn, 'click')
            .subscribe(() => {
                const value = this.input.value;
                if (value.length > 0) {
                    this.input.value = '';
                    this.entries.push(value);
                    this.redraw();
                }
            });
    }

    redraw(): void {
        this.list.innerHTML = '';
        this.draw();
    }
    draw(): void {
        for (const entrie of this.entries) {
            const li = document.createElement('li');
            const textContainer = document.createElement('span');
            textContainer.innerHTML = entrie;
            const btn = document.createElement('button');
            btn.innerHTML = "&times;";

            li.appendChild(textContainer);
            li.appendChild(btn);

            this.list.appendChild(li);
        }
    }


}