interface Girlfriend {
    name: string;
    age: number;
}

enum Gender {
    MALE = "male",
    FEMALE = "female",
    OTHER = "other"
}

export class Person {

    private girlfriend: Girlfriend;

    constructor(private age: number,
        private name: string,
        private gender: Gender = Gender.OTHER) {}

    setGirlfriend(girlfriend: Girlfriend) {
        this.girlfriend = girlfriend
    }

    setGender(gender: Gender): void {
        this.gender = gender;
    }

    setSomething<T>(someParam: T) {

    }
}

const person: Person = new Person(20, 'Pesho');

person.setGender(Gender.MALE);

console.log(person);

console.log(Gender);

namespace test {
    export class Person {

    }
}

const other = new test.Person;