import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-widget',
  templateUrl: './list-widget.component.html',
  styleUrls: ['./list-widget.component.css']
})
export class ListWidgetComponent implements OnInit {

  entries = [];

  inputValue = '';

  editedIndex = -1;

  constructor() { }

  ngOnInit() {
  }

  onSave() {
      if (this.inputValue.length > 0) {
          if (this.entries[this.editedIndex]) {
            this.entries[this.editedIndex] = this.inputValue;
          } else {
            this.entries.push(this.inputValue);
          }

          this.inputValue = '';
      }

      this.editedIndex = -1;
  }


  onDelete(index) {
    if (this.entries[index] !== undefined) {
      this.entries.splice(index, 1);
    }
  }

  onEdit(index) {
    if (this.entries[index] === undefined) {
        return;
    }

    this.inputValue = this.entries[index];
    this.editedIndex = index;
  }
}
